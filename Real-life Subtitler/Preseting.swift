//
//  ViewController.swift
//  Real-life Subtitler
//
//  Created by Peter on 14.06.19.
//

import UIKit

class Preseting: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{

    // UI elements
    @IBOutlet weak var languageSelector: UIPickerView!
    
    // Internal variables
    var speechRecognizer: SpeechRecognitionProtocol?
    var availableLanguages: [Language]?
    var selected: (Language: Int, Region: Int) = (0, 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        speechRecognizer = SiriSpeechRecognizer()
        setAvailableLanguages((speechRecognizer?.getAvailableLocales())!)
        loadSelector()
    }

    @IBAction func subtitleButtonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "toSubtitler", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        setSpeechRecognition()
        do {
            try speechRecognizer?.startRecognition()
        } catch {
            print(error)
        }
        //let subtitler: Subtitler = segue.destination as! Subtitler
        //subtitler.speechRecognizer = speechRecognizer
    }
    
// ******************************* PICKER VIEW *******************************
    
    func loadSelector() {
        self.languageSelector.delegate = self
        self.languageSelector.dataSource = self
    }
    
    // Define the number of columns of data of the pickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    // Set the number of rows of data of the pickerView for each column
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            // Column for languages
            return availableLanguages!.count
        } else {
            // Column for regions
            return (availableLanguages?[selected.Language].regions.count)!
        }
    }
    
    // Set the text for each row of each column
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            // Column for languages
            return availableLanguages?[row].name
        } else {
            // Column for regions
            return availableLanguages?[selected.Language].regions[row].name
        }
    }
    
    // A selection has been done
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            // Language has been changed
            selected = (row, 0)
            languageSelector.reloadComponent(1)
        } else {
            // Region has been changed
            selected.Region = row
        }
    }
    
// **************************** SPEECH RECOGNITION ****************************
    
    func setAvailableLanguages(_ availableLocales: Set<Locale>) {
        availableLanguages = .init()
        var iterator = availableLocales.makeIterator()
        while let loadedLocale = iterator.next() {
            if let loadedLanguageCode = loadedLocale.languageCode, let loadedRegionCode = loadedLocale.regionCode {
                if let index = availableLanguages?.firstIndex(where: {$0.code == loadedLanguageCode}) {
                    availableLanguages?[index].addRegion(regionCode: loadedRegionCode)
                } else {
                    let newLanguage = Language.init(loadedLanguageCode, loadedRegionCode)
                    availableLanguages?.append(newLanguage)
                }
                print("Language \(loadedLanguageCode)_\(loadedRegionCode) available")
            }
        }
        
        availableLanguages = availableLanguages?.sorted(by: {
            $0.name.compare($1.name, locale: Locale.current) == .orderedAscending
        })
    }
    
    func setSpeechRecognition() {
        let selectedLocaleCode = "\(availableLanguages![selected.Language].code)-\(availableLanguages![selected.Language].regions[selected.Region].code)"
        print("Set the speech recognition with the locale \(selectedLocaleCode)")
        speechRecognizer?.setRecognition(locale: Locale (identifier: selectedLocaleCode))
    }
    
}

