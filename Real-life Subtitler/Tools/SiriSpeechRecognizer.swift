//
//  SpeechRecognition.swift
//  Real-life Subtitler
//
//  Created by Peter on 24.06.19.
//

import UIKit

import Speech

class SiriSpeechRecognizer: NSObject, SFSpeechRecognizerDelegate, SpeechRecognitionProtocol {

    var speechRecognizer: SFSpeechRecognizer?
    var audioEngine: AVAudioEngine
    var recognitionRequest: SFSpeechAudioBufferRecognitionRequest
    var recognitionTask: SFSpeechRecognitionTask?
    var inputNode: AVAudioInputNode?
    
    override init() {
        //super.init()
        //locale: Locale(identifier: "en-US")
        audioEngine = AVAudioEngine()
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        speechRecognizer = nil
        inputNode = nil
        //(speechRecognizer as! SFSpeechRecognizer).delegate = self
    }
    
    func getAvailableLocales() -> Set<Locale> {
        return SFSpeechRecognizer.supportedLocales()
    }
    
    func checkPermission() {
        SFSpeechRecognizer.requestAuthorization() { authStatus in
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    print("Acces to speech recognition was accepted")
                case .denied:
                    print("User denied access to speech recognition")
                case .restricted:
                    print("Speech recognition restricted on this device")
                case .notDetermined:
                    print("Speech recognition not yet authorized")
                default:
                    break
                }
            }
        }
    }
    
    func setRecognition(locale: Locale) {
        checkPermission()
        
        speechRecognizer = SFSpeechRecognizer(locale: locale)!
        
        speechRecognizer?.delegate = self
        
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true)
        } catch {
            print(error)
        }
        
        inputNode = audioEngine.inputNode
        
        let recordingFormat = inputNode?.outputFormat(forBus: 0)
        inputNode?.installTap(onBus: 0, bufferSize: 2048, format: recordingFormat) {
            (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest.append(buffer)
        }
        audioEngine.prepare()
    }
    
    func startRecognition() throws {
        do {
            try audioEngine.start()
        } catch {
            print(error)
        }
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        recognitionRequest.shouldReportPartialResults = true
    }
    
    func stopRecognition() {
        audioEngine.stop()
        recognitionRequest.endAudio()
    }
    
    func cancelRecognition() {
        audioEngine.stop()
        recognitionTask?.cancel()
    }
    
}
