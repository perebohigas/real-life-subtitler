//
//  SpeechRecognitionProtocol.swift
//  Real-life Subtitler
//
//  Created by Peter on 13.07.19.
//

import Foundation

protocol SpeechRecognitionProtocol {
    
    func getAvailableLocales() -> Set<Locale>
    
    func checkPermission()
    
    func setRecognition(locale: Locale)
    
    func startRecognition() throws
    
    func stopRecognition()
    
    func cancelRecognition()
    
}
